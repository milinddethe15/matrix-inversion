# Step 1: Use a minimal base image with Python
FROM python:3.10-alpine

# Step 2: Install system dependencies and NumPy using pip
RUN apk --no-cache add \
        gcc \
        musl-dev \
        g++ \
    && pip install numpy \
    && apk del gcc musl-dev g++

# Step 3: Set the working directory in the container
WORKDIR /app

# Step 4: Copy the matrix inversion program into the container
COPY main.py .
