import sys
import numpy as np

def parse_command_line_args():
    # Parses command line arguments and returns the matrix size
    if len(sys.argv) > 1:
        try:
            n = int(sys.argv[1])
            if n < 0:
                print("Error: Size of the matrix must be a non-negative integer.")
                sys.exit(1)
            return n
        except ValueError:
            print("Error: Please enter a valid integer for the size of the matrix.")
            sys.exit(1)
    else:
        return None

def get_matrix_size_from_user():
    # asks the user for size of matrix and handles the input
    # then returns the size of matrix
    while True:
        try:
            n = int(input("Enter the size of the matrix (N): "))
            if n < 0:
                print("Error: Size of the matrix must be a non-negative integer.")
                
            else:
                return n
        except ValueError:
            print("Error: Please enter a valid integer for the size of the matrix.")
        except KeyboardInterrupt:
            print("\nExiting...")
            sys.exit(1)

def create_random_matrix(N):
    # returns random NxN matrix
    return np.random.rand(N, N)

def invert_matrix(matrix):
    # returns inverted matrix of input matrix
    return np.linalg.inv(matrix)

def main():
    # Check for command-line argument of the size of the matrix
    matrix_size = parse_command_line_args()
    
    if matrix_size is None: 
    # Get user input for the size of the matrix
        matrix_size = get_matrix_size_from_user()
    
    # Create a random matrix
    random_matrix = create_random_matrix(matrix_size)

    # Invert the matrix
    inverted_matrix = invert_matrix(random_matrix)
    
    print("Random Matrix:")
    print(random_matrix)
    print("\nInverted Matrix:")
    print(inverted_matrix)

if __name__ == "__main__":
    main()
