# Matrix Inversion

This Python program utilizes [NumPy](https://numpy.org) to generate a random NxN matrix and then invert it. The size of the matrix is provided by the user.

User can enter the matrix size to the program by two ways:
1. Using command line argument, or
2. In-program input prompt.
<div  align="center">
<img src="img/program-workflow.png"/>
<p> Program Workflow</p>
</div>

## Features

- Generates a random NxN matrix using [NumPy.random.rand](https://numpy.org/doc/stable/reference/random/generated/numpy.random.rand.html#numpy-random-rand).
- Calculates the inverse of the generated matrix using [linear algebra inversion](https://numpy.org/doc/stable/reference/generated/numpy.linalg.inv.html#numpy-linalg-inv).
- Allows user input for the size of the matrix.
- Prints both the random matrix and its inverted form.

## Usage

1. Clone this repository to your local machine or copy the main.py code to a online python compiler.
2. Make sure you have Python and NumPy installed.
3. Run the program by executing the following command in your terminal:

    ```bash
    python main.py {matrix_size}
    ```

4. Enter the size of the matrix.
5. The program will output the randomly generated matrix and its inverted form.

## Dockerfile

The Dockerfile allows build a Docker image that includes Python and NumPy to run the matrix inversion program.
The chosen base image is **python:3.10-alpine** (keeping image size at higher importance).
Further for reduction of docker image size, the [build dependencies](https://numpy.org/devdocs/building/) is removed (gcc, musl-dev, g++) after installing NumPy.

Ref: [Choosing the Right Python Docker Image: Slim Buster vs. Alpine vs. Slim Bullseye](https://medium.com/@arif.rahman.rhm/choosing-the-right-python-docker-image-slim-buster-vs-alpine-vs-slim-bullseye-5586bac8b4c9)

Here's how you can use it:

### Build the Docker image

```bash
# run in project directory
docker build -t matrix-inversion-image .
```

### Run the Docker image

```bash
docker run matrix-inversion-image python main.py {matrix-size}
```
## Pipeline

The pipeline automates the process of building and running a python program for matrix inversion with specified matrix size(5000).

There are two stages: 
1. build: Builds the docker image containing the python program and push it in gitlab [container registry](https://docs.gitlab.com/ee/user/packages/container_registry/).
2. run: Executes the matrix inversion program on both Linux and macOS environments.

The run stage has two jobs:
1. on_linux
2. on_macos

which gives the execution time using the [time](https://www.javatpoint.com/linux-time) command available on both linux and macOS environments.
In the time output,
```
real: represents the actual clock time it took for the command to execute.
user: is the amount of CPU time spent in user mode.
sys: is the amount of CPU time spent in kernel mode.
```

To use the specific [runner](https://docs.gitlab.com/runner/) on gitlab, each job should be tagged with specific runner tag to work on. (https://docs.gitlab.com/ee/ci/yaml/index.html#tags)

I have used `saas-linux-medium-amd64` for linux (btw gitlab uses linux if no runner is mentioned) and `saas-macos-medium-m1` for macOS.

In the CI, I have used [Predefined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) by Gitlab to set the registry User and Password.

For macOS environments, Gitlab premium is needed. (https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)

## Support for Multi-Architecture versions of Docker image

### Interactive Workflow:

1. Using Docker Buildx, we can build images for Multi-Architecture platforms. (https://github.com/docker/buildx?tab=readme-ov-file#building-multi-platform-images)

2. Start Builder instance: 
```bash
docker buildx create --name buildx_instance --use
```
Instance can be checked using `docker buildx ls`.

3. Using `--platform` flag to specify the target platforms for the build:
```bash
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t matrix-inversion-image --push .
```
Different platfrom options:

    `linux/amd64`: 64-bit x86 architecture.
    
    `linux/arm64`: 64-bit ARM architecture.
    
    `linux/arm/v7`: 32-bit ARM architecture, specifically ARMv7.

4. The built image is pushed to Docker registry specified by the `--push` flag.

For more specific build, creating a `makefile` having seperate config of different builds can be handy:
```makefile
build-amd64:
	docker buildx build --platform linux/amd64 --t "matrix-inversion-image-amd64" --push .

build-arm64:
	docker buildx build --platform linux/arm64 --t "matrix-inversion-image-arm64" --push .
```

### CI Workflow:

CI config File:

```yml
stages:
  - build-push-image

build-push-image:
  stage: build-push-image
  image: docker:stable
  services:
    - docker:stable-dind
  before_script:
    - docker info
  script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
    - docker context create builder
    - docker buildx create builder --use
    - docker buildx build --platform linux/amd64,linux/arm64 -t $CI_REGISTRY_IMAGE --push .
```

This CI workflow can be made responsive to build for specific platform by making individual jobs for each platforms which will be triggered, if required, by the user.

ref: (https://forum.gitlab.com/t/its-2023-how-do-i-build-multiplatform-docker-images-in-my-gitlab-com-ci-pipeline/83972)